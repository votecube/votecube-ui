import {
	IsData,
	IsDelta,
} from '../core/common'

export type UiDocStatus = IsData | IsDelta
