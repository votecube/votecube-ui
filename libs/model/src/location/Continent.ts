import {Continent_Id} from '@votecube/ecclesia'

export const CONTINENTS = 'CONTINENTS'

export interface IContinent {

	id: Continent_Id
	name: string

}
