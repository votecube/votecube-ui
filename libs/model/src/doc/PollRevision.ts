import {IsDoc}          from '../core/common'
import {ICoreRevision} from '../core/PollRevision'

export interface IPollRevisionDoc
	extends ICoreRevision<IsDoc> {
}
