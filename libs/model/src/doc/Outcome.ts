import {IsDoc}        from '../core/common'
import {ICoreOutcome} from '../core/Outcome'

export interface IOutcomeDoc
	extends ICoreOutcome<IsDoc> {
}
