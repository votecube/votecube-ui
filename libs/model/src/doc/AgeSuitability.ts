import {
	ICoreAgeSuitabilityTracked,
	IsDoc
} from '../core/core'

interface IAgeSuitabilityTrackedDoc
	extends ICoreAgeSuitabilityTracked<IsDoc> {
}
