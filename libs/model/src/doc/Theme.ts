import {IsDoc}                      from '../core/common'
import {ICoreAgeSuitabilityTracked} from '../core/DocumentValue'

export interface IThemeDoc
	extends ICoreAgeSuitabilityTracked<IsDoc> {
}
