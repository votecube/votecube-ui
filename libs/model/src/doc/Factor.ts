import {IsDoc}       from '../core/common'
import {ICoreFactor} from '../core/Factor'


export interface IFactorDoc
	extends ICoreFactor<IsDoc> {
}
