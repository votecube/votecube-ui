import {
	ICoreAgeSuitabilityTracked,
	IsDelta
} from '../core/core'

interface IAgeSuitabilityTrackedDelta
	extends ICoreAgeSuitabilityTracked<IsDelta> {
}
