import {
	ICoreAgeSuitabilityTracked,
	IsData
} from '../core/core'

interface IAgeSuitabilityTrackedData
	extends ICoreAgeSuitabilityTracked<IsData> {
}
