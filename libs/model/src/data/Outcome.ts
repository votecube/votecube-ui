import {IsData}       from '../core/common'
import {ICoreOutcome} from '../core/Outcome'

export interface IOutcomeData
	extends ICoreOutcome<IsData> {
}
