import {ICoreTheme}                 from '../core/core'
import {IsData}                     from '../core/common'

export interface IThemeData
	extends ICoreTheme<IsData> {
}
