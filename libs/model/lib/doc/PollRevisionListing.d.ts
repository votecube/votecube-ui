import { IsDoc } from '../core/common';
import { ICoreRevisionListing } from '../core/PollRevisionListing';
export interface IPollRevisionListingDoc extends ICoreRevisionListing<IsDoc> {
}
//# sourceMappingURL=PollRevisionListing.d.ts.map