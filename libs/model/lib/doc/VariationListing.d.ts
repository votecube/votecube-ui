import { IsDoc } from '../core/common';
import { ICoreRevisionListing } from '../core/RevisionListing';
export interface IRevisionListingDoc extends ICoreRevisionListing<IsDoc> {
}
