import { IsDoc } from '../core/common';
import { ICoreFactorPoll, ICoreOutcomePoll, ICorePoll, ICorePositionPoll } from '../core/Poll';
export interface IPollDoc extends ICorePoll<IsDoc> {
}
export interface IFactorPollDoc extends ICoreFactorPoll<IsDoc> {
}
export interface IOutcomePollDoc extends ICoreOutcomePoll<IsDoc> {
}
export interface IPositionPollDoc extends ICorePositionPoll<IsDoc> {
}
//# sourceMappingURL=Poll.d.ts.map