import { IsDoc } from '../core/common';
import { ICoreFactorPosition, ICorePosition } from '../core/Position';
export interface IPositionDoc extends ICorePosition<IsDoc> {
}
export interface IFactorPositionDoc extends ICoreFactorPosition<IsDoc> {
}
//# sourceMappingURL=Position.d.ts.map