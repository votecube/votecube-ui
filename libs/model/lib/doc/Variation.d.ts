import { IsDoc } from '../core/common';
import { ICoreRevision } from '../core/Revision';
export interface IRevisionDoc extends ICoreRevision<IsDoc> {
}
