import { IsData } from '../core/common';
import { ICoreRevisionListing } from '../core/PollRevisionListing';
export interface IPollRevisionListingData extends ICoreRevisionListing<IsData> {
}
//# sourceMappingURL=PollRevisionListing.d.ts.map