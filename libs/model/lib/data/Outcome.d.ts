import { IsData } from '../core/common';
import { ICoreOutcome } from '../core/Outcome';
export interface IOutcomeData extends ICoreOutcome<IsData> {
}
//# sourceMappingURL=Outcome.d.ts.map