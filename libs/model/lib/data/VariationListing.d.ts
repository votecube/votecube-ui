import { IsData } from '../core/common';
import { ICoreRevisionListing } from '../core/RevisionListing';
export interface IRevisionListingData extends ICoreRevisionListing<IsData> {
}
