import { ICoreTheme } from '../core/core';
import { IsData } from '../core/common';
export interface IThemeData extends ICoreTheme<IsData> {
}
//# sourceMappingURL=Theme.d.ts.map