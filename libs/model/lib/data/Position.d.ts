import { IsData } from '../core/common';
import { ICoreFactorPosition, ICorePosition } from '../core/Position';
export interface IPositionData extends ICorePosition<IsData> {
}
export interface IFactorPositionData extends ICoreFactorPosition<IsData> {
}
//# sourceMappingURL=Position.d.ts.map