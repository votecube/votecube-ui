import { Continent_Id } from '@votecube/ecclesia';
export declare const CONTINENTS = "CONTINENTS";
export interface IContinent {
    id: Continent_Id;
    name: string;
}
//# sourceMappingURL=Continent.d.ts.map