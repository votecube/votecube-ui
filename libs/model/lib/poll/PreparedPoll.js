export const POLLS = 'POLLS';
export const POLL_COLS = [
    ['createdAt', 'ca'],
    ['endDate', 'ed'],
    ['id', 'pid', 1],
    ['name', 'n'],
    ['startDate', 'sd'],
    ['theme', 'tid', 'T'],
    ['user', 'uai', 'UA'],
    ['parentPoll', 1, 'ppi', 'P']
];
//# sourceMappingURL=PreparedPoll.js.map