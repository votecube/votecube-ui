import { ITweenVoteFactor, IVoteFactor } from './VoteFactor';
export interface IVote {
    1: IVoteFactor;
    2: IVoteFactor;
    3: IVoteFactor;
}
export interface ITweenVote {
    1: ITweenVoteFactor;
    2: ITweenVoteFactor;
    3: ITweenVoteFactor;
}
//# sourceMappingURL=Vote.d.ts.map