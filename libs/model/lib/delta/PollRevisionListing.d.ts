import { IsDelta } from '../core/common';
import { ICoreRevisionListing } from '../core/PollRevisionListing';
export interface IPollRevisionListingDelta extends ICoreRevisionListing<IsDelta> {
}
//# sourceMappingURL=PollRevisionListing.d.ts.map