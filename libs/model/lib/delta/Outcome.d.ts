import { IsDelta } from '../core/common';
import { ICoreOutcome } from '../core/Outcome';
export interface IOutcomeDelta extends ICoreOutcome<IsDelta> {
}
//# sourceMappingURL=Outcome.d.ts.map