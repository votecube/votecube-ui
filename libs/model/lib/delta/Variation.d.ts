import { IsDelta } from '../core/common';
import { ICoreRevision } from '../core/Revision';
export interface IRevisionDelta extends ICoreRevision<IsDelta> {
}
