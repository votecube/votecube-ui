import { IsDelta } from '../core/common';
import { ICoreRevision } from '../core/PollRevision';
export interface IPollRevisionDelta extends ICoreRevision<IsDelta> {
}
//# sourceMappingURL=PollRevision.d.ts.map