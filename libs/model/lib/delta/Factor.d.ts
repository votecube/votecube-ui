import { IsDelta } from '../core/common';
import { ICoreFactor } from '../core/Factor';
export interface IFactorDelta extends ICoreFactor<IsDelta> {
}
//# sourceMappingURL=Factor.d.ts.map