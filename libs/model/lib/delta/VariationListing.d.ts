import { IsDelta } from '../core/common';
import { ICoreRevisionListing } from '../core/RevisionListing';
export interface IRevisionListingDelta extends ICoreRevisionListing<IsDelta> {
}
