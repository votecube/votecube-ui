import { IsDelta } from '../core/common';
import { ICoreAgeSuitabilityTracked } from '../core/DocumentValue';
export interface IThemeDelta extends ICoreAgeSuitabilityTracked<IsDelta> {
}
//# sourceMappingURL=Theme.d.ts.map