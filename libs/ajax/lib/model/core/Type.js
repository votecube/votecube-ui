"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Type;
(function (Type) {
    Type[Type["POLL"] = 1] = "POLL";
    Type[Type["DIMENSION"] = 2] = "DIMENSION";
    Type[Type["DIRECTION"] = 3] = "DIRECTION";
})(Type = exports.Type || (exports.Type = {}));
//# sourceMappingURL=Type.js.map