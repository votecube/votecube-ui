"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mode;
(function (Mode) {
    Mode[Mode["NULL"] = 0] = "NULL";
    Mode[Mode["RECORD"] = 1] = "RECORD";
    Mode[Mode["REFERENCE"] = 2] = "REFERENCE";
})(Mode = exports.Mode || (exports.Mode = {}));
//# sourceMappingURL=Mode.js.map