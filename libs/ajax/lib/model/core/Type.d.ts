export declare enum Type {
    POLL = 1,
    DIMENSION = 2,
    DIRECTION = 3
}
