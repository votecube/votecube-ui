/**
 * https://stackoverflow.com/questions/8482309/converting-javascript-integer-to-byte-array-and-back
 *
 * MODIFIED
 */
import { ICursor } from './Cursor';
export declare function bytesToNaturalNum(bytes: Uint8Array, cursor: ICursor, length: number): number;
/**
 * https://stackoverflow.com/questions/8936984/uint8array-to-string-in-javascript
 */
export declare function utf8BytesToStr(bytes: Uint8Array, length: number): string;
