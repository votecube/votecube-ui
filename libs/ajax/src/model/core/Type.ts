export enum Type {
	POLL      = 0x01,
	DIMENSION = 0x02,
	DIRECTION = 0x03,
}
