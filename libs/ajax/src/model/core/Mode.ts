export enum Mode {
	NULL      = 0x00,
	RECORD    = 0x01,
	REFERENCE = 0x02,
}
