import { IFormFactory } from './FormFactory';
export declare const forms: import("@airport/di").ILibrary;
export declare const FORM_FACTORY: import("@airport/di").IDiToken<IFormFactory>;
//# sourceMappingURL=tokens.d.ts.map