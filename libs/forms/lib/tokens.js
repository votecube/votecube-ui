import { system } from '@airport/di';
export const forms = system('votecube-ui').lib('forms');
export const FORM_FACTORY = forms.token('IFormFactory');
//# sourceMappingURL=tokens.js.map