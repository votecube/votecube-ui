import { LabelRule } from './field/Field';
export { FragmentType } from './field/date/DateFragments';
export { LabelRule } from './field/Field';
export * from './tokens';
export * from './FormFactory';
export const CONSTS = {
    rules: {
        label: LabelRule
    }
};
//# sourceMappingURL=index.js.map