import { Route_ParamValue, Route_Path } from './Routes';
export declare const ABOUT: Route_Path;
export declare const FEEDBACK: Route_Path;
export declare const RELEASE_PLAN: Route_Path;
export declare const FACTOR_INFO_MAIN: Route_Path;
export declare const FACTOR_LIST: Route_Path;
export declare const POLL_LIST: Route_Path;
export declare const VARIATION_LIST: Route_Path;
export declare const POLL_FORM: Route_Path;
export declare const POLL_MAIN: Route_Path;
export declare const POLL_LOCATIONS: Route_Path;
export declare const POLL_TIME_FRAME: Route_Path;
export declare const CARD_CLIMATE_CHANGE: Route_Path;
export declare function navigateToPage(routePath: Route_Path, paramMap?: {
    [paramName: string]: Route_ParamValue;
}): void;
//# sourceMappingURL=routing.d.ts.map